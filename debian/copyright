Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xmldom
Upstream-Contact: http://github.com/jindw/xmldom/issues
Source: https://github.com/jindw/xmldom
comment: package.json said LGPL-2 or MIT (expat according to link in LICENSE)

Files: *
Copyright: 2013-2017 jindw <jindw@xidea.org> (http://www.xidea.org)
    2013-2017 Yaron Naveh <yaronn01@gmail.com> (http://webservices20.blogspot.com/)
    2013-2017 Harutyun Amirjanyan <amirjanyan@gmail.com> (https://github.com/nightwing)
    2013-2017 Alan Gutierrez <alan@prettyrobots.com>  (http://www.prettyrobots.com/)
License: LGPL-2 or Expat

Files: debian/*
Copyright: 2018 Bastien Roucariès <rouca@debian.org>
License: LGPL-2 or Expat

License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.